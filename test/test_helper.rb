

require_relative '../lib/random_code'
require_relative '../lib/secret_url'
require_relative '../lib/tiny_url'
require_relative '../lib/has_code'

require 'active_record'

database_adaptor       = ENV['CI_DB_TYPE'] || 'sqlite'
database_configuration = ENV['CI_DB_CONFIG'] || File.expand_path("../database.yml", __FILE__)

ActiveRecord::Base.configurations = YAML.safe_load(IO.read(database_configuration))
ActiveRecord::Base.establish_connection(database_adaptor.to_sym)
ActiveRecord::Schema.verbose = false

def drop_tables
  tables = ActiveRecord::Base.connection.data_sources
  tables.each do |table|
    ActiveRecord::Base.connection.drop_table(table)
  end
end