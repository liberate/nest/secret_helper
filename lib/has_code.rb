module HasCode

  module DefineHasCodeMethod
    def has_code(configuration={})
      before_validation :generate_code, on: :create
      class_attribute :has_code_config
      self.has_code_config = {length: 6, type: :alphanum}.merge(configuration)
      extend(HasCode::ClassMethods)
      include(HasCode::InstanceMethods)
    end
  end

  module ClassMethods
    def find_by_encrypted_code(code)
      if !code.present?
        return nil
      else
        code = SecretUrl::decrypt(code.strip)&.split('|')&.first
        return find_by_code(code)
      end
    end

    def find_by_any_code(code)
      if !code.present?
        return nil
      elsif code =~ /^[A-Z0-9]{#{self.has_code_config[:length]}}$/i
        return find_by_code(code.upcase)
      else
        return find_by_encrypted_code(code)
      end
    end

    def is_a_code?(code)
      code =~ /\A[A-Z0-9]{#{self.has_code_config[:length]}}\z/
    end

    def from_param(code)
      find_by_code(code)
    end
  end

  module InstanceMethods
    def to_param
      code
    end

    def to_encrypted_param
      SecretUrl::encrypt(code)
    end

    def generate_code
      begin
        self.code = if self.class.has_code_config[:type] == :numeric
          rand(10**self.class.has_code_config[:length]).to_s.rjust(self.class.has_code_config[:length], "0")
        else
          RandomCode.create(self.class.has_code_config[:length]).upcase
        end
      end until self.class.find_by_code(self.code).nil?
    end
  end
end
