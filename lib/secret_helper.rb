module SecretHelper
  class Engine < ::Rails::Engine
    config.autoload_paths << File.expand_path('..', __FILE__)

    config.to_prepare do
      ActiveSupport.on_load(:active_record) do
        extend HasCode::DefineHasCodeMethod
      end
    end
  end
end