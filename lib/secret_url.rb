require 'rbnacl'

#
# NOTE: for SimpleBox, the key is RbNaCl::SecretBox.key_bytes (which is 32 bytes)
#
# So, our configured secret should be exactly 64 hex digits
#
# This key has 256 bits of entropy
#

module SecretUrl
  def self.encrypt(cleartext, secret_name = :secret_key_base)
    key = binary_secret(secret_name)
    box = RbNaCl::SimpleBox.from_secret_key(key)
    ciphertext = box.encrypt(cleartext.to_s)
    return Base64.urlsafe_encode64(ciphertext).gsub('=', '')
  rescue
    return nil
  end

  def self.decrypt(url_text, secret_name = :secret_key_base)
    key = binary_secret(secret_name)
    box = RbNaCl::SimpleBox.from_secret_key(key)
    ciphertext = Base64.urlsafe_decode64(url_text)
    cleartext = box.decrypt(ciphertext)
    return cleartext
  rescue
    return nil
  end

  #
  # encoded secrets contain:
  #
  # 0: object id
  # 1: path
  # 2: user id
  # 4: expiration
  #
  # each parameter is optional, but it must contain at least one
  #
  def self.encode(path: nil, expires: nil, id: nil, user: nil, auth: nil)
    str = [id, path, user&.id, auth, expires&.to_i].join('|')
    encrypt(str)
  end

  def self.decode(ciphertext)
    if ciphertext.present?
      cleartext = decrypt(ciphertext.strip)
      if cleartext
        if cleartext =~ /\|/
          id, path, user_id, auth, expire_time_t = cleartext.split('|')
          user   = User.find_by_id(user_id)
          expires = expire_time_t ? Time.at(expire_time_t.to_i) : nil
          retval = {
            id: id,
            path: path,
            user: user,
            auth: auth,
            expires: expires
          }
        else
          return {id: cleartext}
        end
      end
    else
      return {}
    end
  end

  def self.binary_secret(secret_name)
    error = nil
    value = get_configuration_value(secret_name)
    if value.nil?
      error = "ERROR: the secret '#{secret_name}' is not defined in environment or credentials.yml.enc"
    elsif value !~ /\A[a-fA-F0-9]{64,}\z/
      error = "ERROR: the secret '#{secret_name}' MUST be at least 64 characters hex"
    end
    if error
      Rails.logger.error(error) if defined?(Rails)
      raise ArgumentError, error
    end
    return [value[0..63]].pack('H*')
  end

  private

  def self.get_configuration_value(name)
    name = name.to_s
    ENV[name.upcase] ||
    ENV[name] ||
    defined?(Rails) && Rails.application.credentials.send(name) ||
    defined?(Conf) && Conf.data[name]
  end
end
